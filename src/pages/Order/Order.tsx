import { Col, Row, Space, Table, Tag } from "antd";
import CusButton from "../../components/CusButton/CusButton";
import { ColumnsType } from "antd/es/table";
import { IOrder, getAllOrder } from "../../redux/features/orderSlice";
import { useAppDispatch } from "../../redux/store";
import { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { formatISODateToYYYYMMDD, formattedMoney } from "../../config/help";
import ModalDetailOrder from "./ModalDetailOrder";

const Order = () => {
  const dispatch = useAppDispatch();
  const [showDetail, setShowDetail] = useState(false);
  const [dataOrder, setDataOrder] = useState();
  const [currentPage, setCurrentPage] = useState(1);

  const columns: ColumnsType<IOrder> = [
    {
      title: "Người dùng",
      render: (_, record) => <span>{record?.info.info.name}</span>,
    },
    {
      title: "Ngày tạo đơn",
      render: (_, record) => (
        <span>{formatISODateToYYYYMMDD(record?.createdAt)}</span>
      ),
    },
    {
      title: "Giá trị đơn",
      render: (_, record) => (
        <span>{formattedMoney(record?.cart.cartTotal)}₫</span>
      ),
    },
    {
      title: "Tình trạng",
      dataIndex: "orderStatus",
      key: "orderStatus",
      align: "center",
      render: (text, record) => (
        <Tag color={getStatusColor(record.orderStatus)} key={text}>
          {text.toUpperCase()}
        </Tag>
      ),
    },
    {
      title: "Thanh toán",
      render: (_, record) => (
        <span>
          {record?.paymentIntent === "Unpaid"
            ? "Chưa thanh toán"
            : "Đã thanh toán"}
        </span>
      ),
      align: "center",
    },
    {
      title: "Action",
      key: "action",
      render: (_, record) => (
        <Space size="middle">
          <CusButton
            label="Xem chi tiết"
            onClick={() => handleShowDetail(record)}
          />
        </Space>
      ),
      align: "center",
    },
  ];

  const getStatusColor = (status: string) => {
    if (status === "Processing") {
      return "blue";
    }
    if (status === "Shipping") {
      return "blue";
    }
    if (status === "Cancelled") {
      return "volcano";
    }
    if (status === "Completed") {
      return "green";
    }
  };

  const handleShowDetail = async (data: any) => {
    await setDataOrder(data);
    setShowDetail(true);
  };

  useEffect(() => {
    dispatch(getAllOrder(currentPage));
  }, [currentPage]);

  const { orders, isLoading } = useSelector((state: any) => state.order);
  return (
    <>
      {isLoading && <div id="preloader"></div>}
      <Row className="m-2 mb-4">
        <Col span={20}>
          <h1 className="title-page">Danh Sách Đơn Hàng</h1>
        </Col>
      </Row>
      <Table
        columns={columns}
        dataSource={orders.orders}
        rowKey={(record) => record._id}
        pagination={{
          pageSize: 10,
          current: currentPage,
          onChange: (e) => {
            setCurrentPage(e);
          },
          total: orders?.count,
          showSizeChanger: false,
        }}
      />
      <ModalDetailOrder
        isOpen={showDetail}
        dataOrder={dataOrder}
        onCancel={() => setShowDetail(false)}
        currentPage={currentPage}
      />
    </>
  );
};

export default Order;
