import { Modal, Table } from "antd";
import { formatISODateToYYYYMMDD, formattedMoney } from "../../config/help";
import { ColumnsType } from "antd/es/table";
import "./Order.scss";
import CustomInputSelect from "../../components/CustomInputSelect/CustomInputSelect";
import orderService from "../../redux/api/orderService";
import { getAllOrder } from "../../redux/features/orderSlice";
import { useAppDispatch } from "../../redux/store";

const columnsCart: ColumnsType<any> = [
  {
    title: "Sản Phẩm",
    width: "35%",
    key: "title",
    render: (_, item) => (
      <div className="title">
        <span>{item.name}</span>
      </div>
    ),
  },
  {
    title: "Màu sắc",
    key: "color",
    align: "center",
    render: (_, item) => <span>{item.color}</span>,
  },
  {
    title: "Số Lượng",
    key: "quantity",
    align: "center",
    render: (_, item) => <span>{item.quantity}</span>,
  },
  {
    title: "Thành Tiền",
    key: "subTotal",
    align: "center",
    render: (_, item) => (
      <span>{formattedMoney(item.price * item.quantity)}₫</span>
    ),
  },
];

const ModalDetailOrder = (props: any) => {
  const { dataOrder, isOpen, onCancel, currentPage } = props;
  const dispatch = useAppDispatch();

  const handleChangeStatus = async (value: string) => {
    if (dataOrder) {
      await orderService.changeOrderStatus(dataOrder?._id, {
        orderStatus: value,
      });
      dispatch(getAllOrder(currentPage));
    }
  };

  const handleChangePay = async (value: string) => {
    if (dataOrder) {
      await orderService.changeOrderStatus(dataOrder?._id, {
        paymentIntent: value,
      });
      dispatch(getAllOrder(currentPage));
    }
  };

  return (
    <Modal
      open={isOpen}
      footer={false}
      onCancel={onCancel}
      maskClosable={false}
      width={1000}
      className="order"
      style={{ top: 30 }}
    >
      <h4>{`Đơn hàng ${dataOrder?._id}`}</h4>
      <h5 className="mt-3">Thông tin về đơn hàng</h5>
      <div className="d-flex gap-10">
        <span className="label">Tên người nhận: </span>
        <span className="content">{dataOrder?.info.info.name}</span>
      </div>
      <div className="d-flex gap-10">
        <span className="label">Email: </span>
        <span className="content">{dataOrder?.info.info.email}</span>
      </div>
      <div className="d-flex gap-10">
        <span className="label">Số điện thoại: </span>
        <span className="content">{dataOrder?.info.info.phone}</span>
      </div>
      <div className="d-flex gap-10">
        <span className="label">Địa chỉ nhận hàng: </span>
        <span className="content">
          {dataOrder?.info.info.fullAddress.province},{" "}
          {dataOrder?.info.info.fullAddress.district},{" "}
          {dataOrder?.info.info.fullAddress.ward},{" "}
          {dataOrder?.info.info.fullAddress.address}
        </span>
      </div>
      <div className="d-flex gap-10">
        <span className="label">Ngày tạo đơn hàng: </span>
        <span className="content">
          {formatISODateToYYYYMMDD(dataOrder?.createdAt)}
        </span>
      </div>
      <div className="d-flex gap-10">
        <span className="label">Tình trạng đơn hàng: </span>
        <span className="content">{dataOrder?.orderStatus}</span>
      </div>
      <div className="d-flex gap-10">
        <span className="label">Thanh toán: </span>
        <span className="content">
          {dataOrder?.paymentIntent === "Unpaid"
            ? "Chưa thanh toán"
            : "Đã thanh toán"}
        </span>
      </div>
      <h5 className="mt-3">Thông tin về sản phẩm trong đơn hàng</h5>
      <Table
        columns={columnsCart}
        dataSource={dataOrder?.cart.products}
        rowKey={"id"}
      />
      <div className="d-flex gap-10">
        <span className="label">Tổng giá tiền đơn hàng: </span>
        <span className="content">
          {formattedMoney(dataOrder?.cart.cartTotal)}₫
        </span>
      </div>
      {dataOrder?.info.coupon !== null && (
        <>
          <div className="d-flex gap-10">
            <span className="label">Mã giảm giá: </span>
            <span className="content">
              {dataOrder?.info.coupon.name}({dataOrder?.info.coupon.discount}%)
            </span>
          </div>
          <div className="d-flex gap-10">
            <span className="label">Tổng giá tiền sau giảm giá: </span>
            <span className="content">
              {formattedMoney(
                (dataOrder?.cart.cartTotal *
                  (100 -
                    (dataOrder?.info.coupon?.discount
                      ? dataOrder?.info.coupon.discount
                      : 0))) /
                  100
              )}
              ₫
            </span>
          </div>
        </>
      )}
      <div className="mt-4 d-flex items-center">
        <span className="label">Chuyển trạng thái đơn hàng:</span>
        <CustomInputSelect
          defaultValue={dataOrder?.orderStatus}
          onChange={handleChangeStatus}
          options={[
            { value: "Processing", label: "Processing" },
            { value: "Shipping", label: "Shipping" },
            { value: "Cancelled", label: "Cancelled" },
            { value: "Completed", label: "Completed" },
          ]}
        />
      </div>
      <div className="mt-4 d-flex items-center">
        <span className="label">Tình trạng thanh toán:</span>
        <CustomInputSelect
          defaultValue={dataOrder?.paymentIntent}
          onChange={handleChangePay}
          options={[
            { value: "Unpaid", label: "Chưa thanh toán" },
            { value: "Paid", label: "Đã thanh toán" },
          ]}
        />
      </div>
    </Modal>
  );
};

export default ModalDetailOrder;
