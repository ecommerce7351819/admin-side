import { Col, Row, Space, Table } from "antd";
import { ColumnsType } from "antd/es/table";
import { useEffect } from "react";
import { useAppDispatch } from "../../redux/store";
import { getAllBrand } from "../../redux/features/brandSlice";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";
import CusButton from "../../components/CusButton/CusButton";

interface IBrand {
  _id: any;
  title: string;
  img: any;
}

const columns: ColumnsType<IBrand> = [
  {
    title: "Ảnh",
    dataIndex: "img",
    key: "",
    width: "250px",
    render: (_, record) => (
      <img className="img-in-table" src={record?.img.url} alt="" />
    ),
  },
  {
    title: "Tên nhãn hàng",
    dataIndex: "title",
    key: "title",
  },
  {
    title: "Action",
    key: "action",
    render: (_, record) => (
      <Space size="middle">
        <CusButton label="Xem chi tiết" />
        <CusButton onClick={() => {}} label="Xóa" />
      </Space>
    ),
  },
];

const Brand = () => {
  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(getAllBrand());
  }, []);

  const { brands, isLoading } = useSelector((state: any) => state.brand);

  return (
    <>
      {isLoading && <div id="preloader"></div>}
      <Row className="m-2 mb-4">
        <Col span={20}>
          <h1 className="title-page">Danh Sách Nhãn Hàng</h1>
        </Col>
        <Col span={4}>
          <Link to="/add-brand">
            <CusButton label="Thêm nhãn hàng" className="add-new" />
          </Link>
        </Col>
      </Row>
      <Table
        columns={columns}
        dataSource={brands}
        rowKey={(record) => record._id}
      />
    </>
  );
};

export default Brand;
