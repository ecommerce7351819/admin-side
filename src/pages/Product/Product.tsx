import { Col, Pagination, Row, Space, Table } from "antd";
import { ColumnsType } from "antd/es/table";
import React, { useEffect, useState } from "react";
import { useAppDispatch } from "../../redux/store";
import { useSelector } from "react-redux";
import { getAllProduct } from "../../redux/features/productSlice";
import CusButton from "../../components/CusButton/CusButton";
import { Link } from "react-router-dom";
import { formattedMoney } from "../../config/help";

interface IProduct {
  _id: any;
  title: string;
  slug: string;
  description: string;
  price: number;
  category: string;
  brand: string;
  quantity: number;
  sold: number;
  mainImg: any;
  images: [];
  color: [];
  tags: [];
  ratings: [];
  totalRating: string;
  discount: number;
}

const columns: ColumnsType<IProduct> = [
  {
    title: "Sản Phẩm",
    key: "title",
    render: (_, item) => (
      <div className="title">
        <img src={item.mainImg?.url} alt="" />
        <span>{item.title}</span>
      </div>
    ),
  },
  {
    title: "Giá",
    key: "price",
    align: "center",
    render: (_, item) => <span>{formattedMoney(item.price)}₫</span>,
  },
  {
    title: "Đang giảm giá",
    key: "discount",
    align: "center",
    render: (_, item) => <span>{formattedMoney(item.discount)}%</span>,
  },
  {
    title: "Action",
    key: "",
    render: (_, record) => (
      <Space size="middle">
        <Link to={`/product-detail/${record.slug}`}>
          <CusButton label="Xem chi tiết" />
        </Link>
        <CusButton onClick={() => {}} label="Xóa" />
      </Space>
    ),
    align: "center",
  },
];

const Product = () => {
  const dispatch = useAppDispatch();
  const [currentPage, setCurrentPage] = useState(1);

  useEffect(() => {
    dispatch(getAllProduct(currentPage));
  }, [currentPage]);

  const { products, isLoading } = useSelector((state: any) => state.product);

  return (
    <>
      {isLoading && <div id="preloader"></div>}
      <Row className="m-2 mb-4">
        <Col span={20}>
          <h1 className="title-page">Danh Sách Sản Phẩm</h1>
        </Col>
        <Col span={4}>
          <Link to="/add-product">
            <CusButton label="Thêm sản phẩm" className="add-new" />
          </Link>
        </Col>
      </Row>
      <Table
        columns={columns}
        dataSource={products.data}
        rowKey={(record) => record.slug}
        pagination={{
          pageSize: 12,
          current: currentPage,
          onChange: (e) => {
            setCurrentPage(e);
          },
          total: products.productCount,
        }}
      />
    </>
  );
};

export default Product;
