import { useNavigate, useParams } from "react-router-dom";
import { useEffect, useState } from "react";
import productService from "../../redux/api/productService";
import { Button, Col, Form, Row, Select, SelectProps, Space } from "antd";
import CustomInput from "../../components/CustomInput/CustomInput";
import CusInputFile from "../../components/CusInputFile/CusInputFile";
import { MinusCircleOutlined } from "@ant-design/icons";
import { CKEditor } from "@ckeditor/ckeditor5-react";
import ClassicEditor from "@ckeditor/ckeditor5-build-classic";
import CusButton from "../../components/CusButton/CusButton";
import uploadService from "../../redux/api/uploadSevice";
import { useForm } from "antd/es/form/Form";
import { useAppDispatch } from "../../redux/store";
import { getAllBrand } from "../../redux/features/brandSlice";
import { getAllProductCat } from "../../redux/features/productCatSlice";
import { useSelector } from "react-redux";

const DetailProduct = () => {
  const dispatch = useAppDispatch();
  const { slug } = useParams();
  const [product, setProduct] = useState<any>();
  const [form] = useForm();
  const [img, setImg] = useState<any>();
  const [imgs, setImgs] = useState<any>([]);
  const navigate = useNavigate();

  const getAProduct = async () => {
    const res = await productService.getAProduct(slug);
    form.setFieldsValue({ title: res?.title });
    form.setFieldsValue({ slug: res?.slug });
    form.setFieldsValue({ price: res?.price });
    form.setFieldsValue({ discount: res?.discount });
    form.setFieldsValue({ category: res?.category });
    form.setFieldsValue({ brand: res?.brand });
    form.setFieldsValue({ description: res?.description });
    setImg(res?.mainImg);
    setImgs(res?.images);
    setProduct(res);
  };

  const handleUploadImg = async (e: any) => {
    try {
      const res = await uploadService.upload(e.target.files);
      if (res) {
        setImg(res[0]);
      }
      e.target.value = null;
    } catch (error) {}
  };

  const handleUploadImgs = async (e: any) => {
    try {
      const files = Array.from(e.target.files);
      const res = await uploadService.upload(files);
      if (res) {
        setImgs(imgs.concat(res));
      }
      e.target.value = null;
    } catch (error) {}
  };

  const handelDeleteImg = async (id: any) => {
    try {
      setImgs(imgs.filter((item: any) => item.public_id !== id));
      await uploadService.deleteImg(id);
      if (imgs.length === 0) {
        setImgs([]);
      }
    } catch (error) {}
  };

  const stringToSlug = (str: string) => {
    str = str.replace(/^\s+|\s+$/g, ""); // trim
    str = str.toLowerCase();

    // remove accents, swap ñ for n, etc
    var from = "àáäâèéëêìíïîòóöôùúüûñç·/_,:;";
    var to = "aaaaeeeeiiiioooouuuunc------";
    for (var i = 0, l = from.length; i < l; i++) {
      str = str.replace(new RegExp(from.charAt(i), "g"), to.charAt(i));
    }

    str = str
      .replace(/[^a-z0-9 -]/g, "") // remove invalid chars
      .replace(/\s+/g, "-") // collapse whitespace and replace by -
      .replace(/-+/g, "-"); // collapse dashes

    return str;
  };

  const handelGenerateSlug = (e: any) => {
    form.setFieldsValue({ slug: stringToSlug(e.target.value) });
  };

  useEffect(() => {
    getAProduct();
  }, []);

  useEffect(() => {
    form.setFieldsValue({ mainImg: img, images: imgs });
  }, [img, imgs]);

  const handleSubmit = async (value: any) => {
    try {
      const res = await productService.updateProduct(product._id, value);
      if (res?.message === "Done") {
        navigate("/product-list");
      }
    } catch (error) {}
  };

  useEffect(() => {
    dispatch(getAllBrand());
    dispatch(getAllProductCat());
  }, []);

  const { brands } = useSelector((state: any) => state.brand);
  const { productCats } = useSelector((state: any) => state.productCat);

  const options: SelectProps["options"] = [];
  if (productCats) {
    productCats?.map((item: any) => {
      options.push({
        value: item.name,
        label: item.title,
      });
    });
  }
  const allBrand = [] as any[];
  if (brands) {
    brands?.map((item: any) => {
      allBrand.push({
        value: item.title,
        label: item.title,
      });
    });
  }

  return (
    <div className="cus-container">
      <Row className="m-2 mb-4">
        <Col span={24}>
          <h1 className="title-page">Xem Sản Phẩm</h1>
        </Col>
      </Row>
      {product && (
        <Form
          form={form}
          className="cus-form py-2 px-3"
          onFinish={handleSubmit}
        >
          <Row>
            <Col span={12}>
              <h1>Tên sản phẩm</h1>
              <Form.Item
                name="title"
                rules={[
                  { required: true, message: "Tên sản phẩm là bắt buộc" },
                ]}
              >
                <CustomInput
                  onChange={handelGenerateSlug}
                  type="text"
                  className="input-text"
                />
              </Form.Item>
            </Col>
            <Col span={12}>
              <h1>Slug</h1>
              <Form.Item name="slug">
                <CustomInput
                  className="input-text"
                  placeholder="Slug sẽ được tự động tạo ra theo tên sản phẩm"
                  disabled
                />
              </Form.Item>
            </Col>
          </Row>
          <Row className="mt-4">
            <Col span={12}>
              <h1>Danh mục</h1>
              <Form.Item
                name="category"
                rules={[{ required: true, message: "Danh mục là bắt buộc" }]}
              >
                <Select
                  mode="multiple"
                  allowClear
                  style={{ width: "70%" }}
                  placeholder="Chọn danh mục"
                  options={options}
                />
              </Form.Item>
            </Col>
            <Col span={12}>
              <h1>Giá sản phẩm</h1>
              <Form.Item
                name="price"
                rules={[{ required: true, message: "Giá là bắt buộc" }]}
              >
                <CustomInput type="number" className="input-text" />
              </Form.Item>
            </Col>
          </Row>
          <Row className="mt-4">
            <Col span={12}>
              <h1>Nhãn hàng</h1>
              <Form.Item
                name="brand"
                rules={[{ required: true, message: "Nhãn hàng là bắt buộc" }]}
              >
                <Select
                  style={{ width: "70%" }}
                  showSearch
                  placeholder="Chọn nhãn hàng"
                  options={allBrand}
                />
              </Form.Item>
            </Col>
            <Col span={12}>
              <h1>Giảm giá</h1>
              <Form.Item name="discount">
                <CustomInput type="number" className="input-text" />
              </Form.Item>
            </Col>
          </Row>
          <Row className="mt-4">
            <Col span={12}>
              <h1>Ảnh chính</h1>
              <CusInputFile onChange={handleUploadImg} />
              {img && (
                <div className="d-flex mt-3">
                  <div className="position-relative">
                    <img className="show-img" src={img?.url} alt="" />
                    <button
                      type="button"
                      className="btn-del"
                      onClick={() => {
                        uploadService.deleteImg(img?.public_id);
                        setImg(null);
                      }}
                    >
                      <span>X</span>
                    </button>
                  </div>
                </div>
              )}

              <Form.Item name="mainImg">
                <div></div>
              </Form.Item>
            </Col>
            <Col span={12}>
              <h1>Thêm ảnh sản phẩm</h1>
              <CusInputFile onChange={handleUploadImgs} multiple />
              <div className="d-flex mt-3 gap-10 flex-wrap">
                {imgs &&
                  imgs?.map((item: any, j: any) => {
                    return (
                      <div key={j} className="position-relative">
                        <img className="show-img" src={item?.url} alt="" />
                        <button
                          type="button"
                          className="btn-del"
                          onClick={() => {
                            handelDeleteImg(item?.public_id);
                          }}
                        >
                          <span>X</span>
                        </button>
                      </div>
                    );
                  })}
              </div>
              <Form.Item name="images">
                <div></div>
              </Form.Item>
            </Col>
          </Row>
          <Row>
            <Col span={12}>
              <h1>Màu sắc và số lượng</h1>
              <Form.List name="variants" initialValue={product.variants}>
                {(fields, { add, remove }) => (
                  <>
                    {fields.map((field) => (
                      <Space key={field.key} align="baseline" className="my-2">
                        <Form.Item
                          {...field}
                          label="Màu Sắc"
                          name={[field.name, "color"]}
                          rules={[
                            { required: true, message: "Trường Bắt Buộc" },
                          ]}
                        >
                          <CustomInput type="text" />
                        </Form.Item>
                        <Form.Item
                          {...field}
                          label="Số Lượng"
                          name={[field.name, "quantity"]}
                          rules={[
                            { required: true, message: "Trường Bắt Buộc" },
                          ]}
                        >
                          <CustomInput type="number" />
                        </Form.Item>

                        <MinusCircleOutlined
                          onClick={() => remove(field.name)}
                        />
                      </Space>
                    ))}

                    <Form.Item>
                      <Button
                        type="dashed"
                        onClick={() => add()}
                        block
                        className="mt-2"
                        style={{ width: "70%" }}
                      >
                        Thêm màu sắc và số lượng
                      </Button>
                    </Form.Item>
                  </>
                )}
              </Form.List>
            </Col>
            <Col span={12}>
              <h1>Thông số kỹ thuật</h1>
              <Form.List
                name="specifications"
                initialValue={product.specifications}
              >
                {(fields, { add, remove }) => (
                  <>
                    {fields.map((field) => (
                      <Space key={field.key} align="baseline" className="my-2">
                        <Form.Item
                          {...field}
                          label="Loại thông số"
                          name={[field.name, "name"]}
                          rules={[
                            { required: true, message: "Trường Bắt Buộc" },
                          ]}
                        >
                          <CustomInput type="text" />
                        </Form.Item>

                        <Form.Item
                          {...field}
                          label="Thông số"
                          name={[field.name, "content"]}
                          rules={[
                            { required: true, message: "Trường Bắt Buộc" },
                          ]}
                        >
                          <CustomInput type="text" />
                        </Form.Item>

                        <MinusCircleOutlined
                          onClick={() => remove(field.name)}
                        />
                      </Space>
                    ))}

                    <Form.Item>
                      <Button
                        type="dashed"
                        onClick={() => add()}
                        block
                        className="mt-2"
                        style={{ width: "70%" }}
                      >
                        Thêm thông số kỹ thuật
                      </Button>
                    </Form.Item>
                  </>
                )}
              </Form.List>
            </Col>
          </Row>
          <Row className="mt-4">
            <Col span={24}>
              <h1>Mô tả sản phẩm</h1>
              <Form.Item name="description">
                <CKEditor
                  editor={ClassicEditor}
                  onChange={(event, editor) => {
                    const data = editor.getData();
                    form.setFieldsValue({ description: data });
                  }}
                  data={product.description}
                ></CKEditor>
              </Form.Item>
            </Col>
          </Row>
          <Row>
            <Col span={24} className="text-center">
              <CusButton
                htmlType="submit"
                className="btn-add"
                label="Sửa Sản Phẩm"
              />
            </Col>
          </Row>
        </Form>
      )}
    </div>
  );
};

export default DetailProduct;
