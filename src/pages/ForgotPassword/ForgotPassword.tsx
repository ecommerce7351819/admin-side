import { UserOutlined } from "@ant-design/icons";
import { Form } from "antd";
import { Link } from "react-router-dom";
import CustomInput from "../../components/CustomInput/CustomInput";

const ForgotPassword = () => {
  return (
    <div className="py-3 d-flex align-items-center login-page">
      <div
        className="my-5 mx-auto w-35 bg-white p-4 rounded-3 box-shadow"
        style={{ maxWidth: "35vw" }}
      >
        <h3 className="text-center fw-bold">Bạn quên mật khẩu?</h3>
        <p className="text-center">
          Nhập địa chỉ email được liên kết với tài khoản của bạn và chúng tôi sẽ
          gửi một liên kết để đặt lại mật khẩu của bạn
        </p>
        <Form action="">
          <CustomInput
            type="text"
            label="Email"
            id="email"
            prefix={<UserOutlined />}
            className="my-2"
          />
          <button
            className="border-0 px-3 py-2 text-white fw-bold w-100 mt-3"
            type="submit"
            style={{ background: "#ffd333" }}
          >
            Đặt Lại Mật Khẩu
          </button>
        </Form>
        <div className="text-center mt-3 fw-bold">
          <Link to="/">Đăng Nhập</Link>
        </div>
      </div>
    </div>
  );
};

export default ForgotPassword;
