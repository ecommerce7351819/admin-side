import { Col, Form, Row } from "antd";
import React, { useEffect, useState } from "react";
import CustomInput from "../../components/CustomInput/CustomInput";
import CusButton from "../../components/CusButton/CusButton";
import { useForm } from "antd/es/form/Form";
import { useNavigate } from "react-router-dom";
import couponService from "../../redux/api/couponService";

const AddCoupon = () => {
  const navigate = useNavigate();
  const [form] = useForm();

  const handleSubmit = async (value: any) => {
    try {
      const res = await couponService.createCoupon(value);
      if (res?.message === "Success") {
        navigate("/coupons");
      }
    } catch (error) {}
  };

  return (
    <div className="cus-container">
      <Row className="m-2 mb-4">
        <Col span={24}>
          <h1 className="title-page">Thêm Mã Giảm Giá</h1>
        </Col>
      </Row>
      <Form form={form} className="cus-form py-2 px-3" onFinish={handleSubmit}>
        <Row>
          <Col span={12}>
            <h1>Mã giảm giá</h1>
            <Form.Item
              name="name"
              rules={[{ required: true, message: "Mã giảm giá là bắt buộc" }]}
            >
              <CustomInput type="text" className="input-text" />
            </Form.Item>
          </Col>
          <Col span={12}>
            <h1>% giảm giá</h1>
            <Form.Item
              name="discount"
              rules={[{ required: true, message: "% giảm giá là bắt buộc" }]}
            >
              <CustomInput type="number" className="input-text" />
            </Form.Item>
          </Col>
        </Row>
        <Row className="mt-4">
          <Col span={12}>
            <h1>Ngày hết hạn</h1>
            <Form.Item
              name="expiry"
              rules={[{ required: true, message: "Ngày hết hạn là bắt buộc" }]}
            >
              <CustomInput type="date" className="input-text" />
            </Form.Item>
          </Col>
        </Row>
        <Row>
          <Col span={24} className="text-center">
            <CusButton
              htmlType="submit"
              className="btn-add"
              label="Tạo Mã Giảm Giá"
            />
          </Col>
        </Row>
      </Form>
    </div>
  );
};

export default AddCoupon;
