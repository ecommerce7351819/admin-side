import { Col, Row, Space, Table } from "antd";
import { ColumnsType } from "antd/es/table";
import React, { useEffect } from "react";
import { useAppDispatch } from "../../redux/store";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";
import CusButton from "../../components/CusButton/CusButton";
import { getAllCoupon } from "../../redux/features/couponSlice";

interface ICoupon {
  _id: any;
  name: string;
  expiry: Date;
  discount: number;
}

const columns: ColumnsType<ICoupon> = [
  {
    title: "Mã giảm giá",
    dataIndex: "name",
    key: "name",
  },
  {
    title: "Ngày hết hạn",
    dataIndex: "expiry",
    key: "expiry",
  },
  {
    title: "% giảm",
    dataIndex: "discount",
    key: "discount",
  },
  {
    title: "Action",
    key: "action",
    render: (_, record) => (
      <Space size="middle">
        <CusButton label="Xem chi tiết" />
        <CusButton onClick={() => {}} label="Xóa" />
      </Space>
    ),
  },
];

const Coupon = () => {
  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(getAllCoupon());
  }, []);

  const { coupons, isLoading } = useSelector((state: any) => state.coupon);

  return (
    <>
      {isLoading && <div id="preloader"></div>}
      <Row className="m-2 mb-4">
        <Col span={20}>
          <h1 className="title-page">Danh Sách Mã Giảm Giá</h1>
        </Col>
        <Col span={4}>
          <Link to="/add-coupon">
            <CusButton label="Thêm mã giảm giá" className="add-new" />
          </Link>
        </Col>
      </Row>
      <Table
        columns={columns}
        dataSource={coupons}
        rowKey={(record) => record._id}
      />
    </>
  );
};

export default Coupon;
