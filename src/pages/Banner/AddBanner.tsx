import { Col, Form, Row, Switch } from "antd";
import React, { useEffect, useState } from "react";
import CustomInput from "../../components/CustomInput/CustomInput";
import CusButton from "../../components/CusButton/CusButton";
import { useForm } from "antd/es/form/Form";
import { useAppDispatch } from "../../redux/store";
import uploadService from "../../redux/api/uploadSevice";
import CusInputFile from "../../components/CusInputFile/CusInputFile";
import bannerService from "../../redux/api/bannerService";
import { useNavigate } from "react-router-dom";

const AddBanner = () => {
  const dispatch = useAppDispatch();
  const navigate = useNavigate();
  const [form] = useForm();
  const [img, setImg] = useState<any>();
  const [subBanner, setSubBanner] = useState(true);
  const [isShow, setIsShow] = useState(true);

  const handleUploadImg = async (e: any) => {
    try {
      const res = await uploadService.upload(e.target.files);
      if (res) {
        setImg(res[0]);
      }
      e.target.value = null;
    } catch (error) {}
  };

  useEffect(() => {
    form.setFieldsValue({ img });
  }, [img]);

  const handleSubmit = async (value: any) => {
    try {
      const res = await bannerService.createBanner(value);
      if (res?.message === "Success") {
        navigate("/banners");
      }
    } catch (error) {}
  };

  return (
    <div className="cus-container">
      <Row className="m-2 mb-4">
        <Col span={24}>
          <h1 className="title-page">Thêm Banner</h1>
        </Col>
      </Row>
      <Form form={form} className="cus-form py-2 px-3" onFinish={handleSubmit}>
        <Row>
          <Col span={12}>
            <h1>Tên banner</h1>
            <Form.Item
              name="title"
              rules={[{ required: true, message: "Tên nhãn hàng là bắt buộc" }]}
            >
              <CustomInput type="text" className="input-text" />
            </Form.Item>
          </Col>
          <Col span={12}>
            <h1>Action của banner</h1>
            <Form.Item
              name="action"
              rules={[{ required: true, message: "Action là bắt buộc" }]}
            >
              <CustomInput type="text" className="input-text" />
            </Form.Item>
          </Col>
        </Row>
        <Row className="mt-3">
          <Col span={12}>
            <h1>Show banner trên trang chủ</h1>
            <Form.Item name="isShow">
              <Switch
                checkedChildren="Có"
                unCheckedChildren="Không"
                checked={isShow}
                onChange={() => setIsShow(!isShow)}
              />
            </Form.Item>
          </Col>
          <Col span={12}>
            <h1>Banner phụ</h1>
            <Form.Item name="subBanner">
              <Switch
                checkedChildren="Có"
                unCheckedChildren="Không"
                checked={subBanner}
                onChange={() => setSubBanner(!subBanner)}
              />
            </Form.Item>
          </Col>
        </Row>
        <Row className="mt-4">
          <Col span={12}>
            <h1>Ảnh</h1>
            <CusInputFile onChange={handleUploadImg} />
            {img && (
              <div className="d-flex mt-3">
                <div className="position-relative">
                  <img className="show-img" src={img?.url} alt="" />
                  <button
                    type="button"
                    className="btn-del"
                    onClick={() => {
                      uploadService.deleteImg(img?.public_id);
                      setImg(null);
                    }}
                  >
                    <span>X</span>
                  </button>
                </div>
              </div>
            )}

            <Form.Item name="img">
              <div></div>
            </Form.Item>
          </Col>
        </Row>
        <Row>
          <Col span={24} className="text-center">
            <CusButton
              htmlType="submit"
              className="btn-add"
              label="Tạo Nhãn Hàng"
            />
          </Col>
        </Row>
      </Form>
    </div>
  );
};

export default AddBanner;
