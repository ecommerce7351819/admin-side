import { Col, Row, Space, Switch, Table } from "antd";
import { ColumnsType } from "antd/es/table";
import React, { useEffect } from "react";
import { useAppDispatch } from "../../redux/store";
import { getAllBanner } from "../../redux/features/bannerSlice";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";
import CusButton from "../../components/CusButton/CusButton";

interface IBanner {
  _id: any;
  title: string;
  img: any;
  isShow: boolean;
}

const columns: ColumnsType<IBanner> = [
  {
    title: "Ảnh",
    dataIndex: "img",
    key: "",
    width: "250px",
    render: (_, record) => (
      <img className="img-in-table" src={record?.img?.url} alt="" />
    ),
  },
  {
    title: "Tên Banner",
    dataIndex: "title",
    key: "title",
  },
  {
    title: "Đang dùng banner",
    key: "",
    render: (_, record) => (
      <Switch
        checkedChildren="Có"
        unCheckedChildren="Không"
        checked={record.isShow}
      />
    ),
  },
  {
    title: "Action",
    key: "",
    render: (_, record) => (
      <Space size="middle">
        <CusButton onClick={() => {}} label="Xóa" />
      </Space>
    ),
  },
];

const Banner = () => {
  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(getAllBanner());
  }, []);

  const { banners, isLoading } = useSelector((state: any) => state.banner);

  return (
    <>
      {isLoading && <div id="preloader"></div>}
      <Row className="m-2 mb-4">
        <Col span={20}>
          <h1 className="title-page">Danh Sách Banner</h1>
        </Col>
        <Col span={4}>
          <Link to="/add-banner">
            <CusButton label="Thêm banner" className="add-new" />
          </Link>
        </Col>
      </Row>
      <Table
        columns={columns}
        dataSource={banners}
        rowKey={(record) => record._id}
      />
    </>
  );
};

export default Banner;
