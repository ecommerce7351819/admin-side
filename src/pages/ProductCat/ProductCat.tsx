import { Col, Row, Space, Table } from "antd";
import { ColumnsType } from "antd/es/table";
import React, { useEffect } from "react";
import { useAppDispatch } from "../../redux/store";
import { useSelector } from "react-redux";
import CusButton from "../../components/CusButton/CusButton";
import { getAllProductCat } from "../../redux/features/productCatSlice";
import { Link } from "react-router-dom";

interface IProductCat {
  _id: any;
  title: string;
  img: any;
}

const columns: ColumnsType<IProductCat> = [
  {
    title: "Ảnh",
    dataIndex: "img",
    key: "",
    width: "250px",
    render: (_, record) => (
      <img className="img-in-table" src={record.img.url} alt="" />
    ),
  },
  {
    title: "Tên danh mục",
    dataIndex: "title",
    key: "title",
  },
  {
    title: "Action",
    key: "action",
    render: (_, record) => (
      <Space size="middle">
        <CusButton label="Xem chi tiết" />
        <CusButton onClick={() => {}} label="Xóa" />
      </Space>
    ),
  },
];

const ProductCat = () => {
  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(getAllProductCat());
  }, []);

  const { productCats, isLoading } = useSelector(
    (state: any) => state.productCat
  );

  return (
    <>
      {isLoading && <div id="preloader"></div>}
      <Row className="m-2 mb-4">
        <Col span={20}>
          <h1 className="title-page">Danh Sách Danh Mục</h1>
        </Col>
        <Col span={4}>
          <Link to="/add-category">
            <CusButton label="Thêm danh mục" className="add-new" />
          </Link>
        </Col>
      </Row>
      <Table
        columns={columns}
        dataSource={productCats}
        rowKey={(record) => record._id}
      />
    </>
  );
};

export default ProductCat;
