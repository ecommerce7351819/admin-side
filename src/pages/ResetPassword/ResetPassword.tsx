import { LockOutlined } from "@ant-design/icons";
import { Form } from "antd";
import CustomInput from "../../components/CustomInput/CustomInput";

const ResetPassword = () => {
  return (
    <div className="py-3 d-flex align-items-center login-page">
      <div className="my-5 mx-auto w-35 bg-white p-4 rounded-3 box-shadow">
        <h3 className="text-center fw-bold">Đặt Lại Mật Khẩu</h3>
        <p className="text-center">Hãy nhập mật khẩu mới</p>
        <Form action="">
          <CustomInput
            type="password"
            label="Mật Khẩu Mới"
            id="pass"
            prefix={<LockOutlined />}
            className="my-2"
          />
          <CustomInput
            type="password"
            label="Nhập Lại Mật Khẩu Mới"
            id="pass"
            prefix={<LockOutlined />}
            className="my-2"
          />
          <button
            className="border-0 px-3 py-2 text-white fw-bold w-100 mt-3"
            type="submit"
            style={{ background: "#ffd333" }}
          >
            Cập Nhật
          </button>
        </Form>
      </div>
    </div>
  );
};

export default ResetPassword;
