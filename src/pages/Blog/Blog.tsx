import { Col, Row, Space, Table } from "antd";
import { ColumnsType } from "antd/es/table";
import { useEffect } from "react";
import { useAppDispatch } from "../../redux/store";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";
import CusButton from "../../components/CusButton/CusButton";
import { getAllBlog } from "../../redux/features/blogSlice";

interface IBlog {
  _id: any;
  title: string;
  img: any;
}

const columns: ColumnsType<IBlog> = [
  {
    title: "Ảnh",
    dataIndex: "img",
    key: "",
    width: "250px",
    render: (_, record) => (
      <img className="img-in-table" src={record?.img.url} alt="" />
    ),
  },
  {
    title: "Tên Blog",
    dataIndex: "title",
    key: "title",
  },
  {
    title: "Action",
    key: "action",
    render: (_, record) => (
      <Space size="middle">
        <CusButton label="Xem chi tiết" />
        <CusButton onClick={() => {}} label="Xóa" />
      </Space>
    ),
  },
];

const Blog = () => {
  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(getAllBlog());
  }, []);

  const { blogs, isLoading } = useSelector((state: any) => state.blog);

  return (
    <>
      {isLoading && <div id="preloader"></div>}
      <Row className="m-2 mb-4">
        <Col span={20}>
          <h1 className="title-page">Danh Sách Blogs</h1>
        </Col>
        <Col span={4}>
          <Link to="/add-blog">
            <CusButton label="Thêm Blog" className="add-new" />
          </Link>
        </Col>
      </Row>
      <Table
        columns={columns}
        dataSource={blogs}
        rowKey={(record) => record._id}
      />
    </>
  );
};

export default Blog;
