import { MoreOutlined, RiseOutlined, FallOutlined } from "@ant-design/icons";
import { Card, Col, Row } from "antd";
import "./StatisticsCard.scss";
import { formattedMoney } from "../../../config/help";

const StatisticsCard = ({ title, money, compare, ...props }: any) => {
  const caculator = () => {
    if (money > compare) {
      return (((money - compare) / compare) * 100).toFixed(2);
    } else {
      return (((compare - money) / money) * 100).toFixed(2);
    }
  };
  return (
    <>
      <Card className="statistics-card box-shadow">
        <Row className="mb-3">
          <Col
            span={24}
            className="d-flex justify-content-between align-items-center"
          >
            <h2 className="title-card mb-0">{title}</h2>
            <MoreOutlined className="fs-5 btn-more" />
          </Col>
        </Row>
        <Row className="mt-2 d-flex align-items-center">
          <Col span={12}>
            <h2 className="card-value mb-0">{formattedMoney(money)}₫</h2>
          </Col>
          <Col span={12} className="pr-2">
            <div className="">
              {money >= compare ? (
                <div className="text-success d-flex align-items-center justify-content-end gap-5">
                  <RiseOutlined className="text-success" />
                  {caculator()}%
                </div>
              ) : (
                <div className="text-danger d-flex align-items-center justify-content-end gap-5">
                  <FallOutlined />
                  {caculator()}%
                </div>
              )}
            </div>
            <div className="d-flex align-items-center justify-content-end">
              So với tháng trước
            </div>
          </Col>
        </Row>
      </Card>
    </>
  );
};

export default StatisticsCard;
