import { Col, Row } from "antd";
import CustomInputSelect from "../../components/CustomInputSelect/CustomInputSelect";
import CusButton from "../../components/CusButton/CusButton";
import "./Dashboard.scss";
import StatisticsCard from "./StatisticsCard/StatisticsCard";
import Chart from "../../components/Chart/Chart";
import { useEffect, useState } from "react";
import statisticService from "../../redux/api/statisticService";

const Dashboard = () => {
  const [month, setMonth] = useState<any>([]);
  const [thisMonth, setThisMonth] = useState<any>();
  const [lastMonth, setLastMonth] = useState<any>();
  const [dataChart, setDataChart] = useState<any>();
  const [statistic, setStatistic] = useState<any>();

  const getMonth = () => {
    const hienTai = new Date();
    const thangHienTai = hienTai.getMonth();
    const namHienTai = hienTai.getFullYear();
    const thangGanNhat = [];
    for (let i = 1; i <= 12; i++) {
      const thang = (thangHienTai + 1 - i + 12) % 12;
      const nam = namHienTai - (thangHienTai < i - 1 ? 1 : 0);
      thangGanNhat.push({
        value: thang + 1 + "/" + nam,
        label: thang + 1 + "/" + nam,
      });
    }
    setMonth(thangGanNhat);
    setThisMonth(thangGanNhat[0].value);
    setLastMonth(thangGanNhat[1].value);
  };

  const getStatistic = async () => {
    const data = await statisticService.getStatistic();
    setDataChart(data.reverse());
  };

  useEffect(() => {
    getStatistic();
    getMonth();
  }, []);

  const handleChange = (value: any) => {
    setThisMonth(value);
    const thisMonthIndex = month.findIndex((item: any) => item.value === value);
    setLastMonth(month[thisMonthIndex + 1].value);
  };

  useEffect(() => {
    if (thisMonth) {
      getStatisticbyMonth(lastMonth, thisMonth);
    }
  }, [thisMonth]);

  const getStatisticbyMonth = async (lastMonth: any, thisMonth: any) => {
    const data = await statisticService.getStatisticByMonth({
      date1: lastMonth,
      date2: thisMonth,
    });
    setStatistic(data);
  };

  return (
    <>
      <Row className="m-2 mb-4">
        <Col span={12}>
          <h1 className="title-page">Tổng Quan Số Liệu</h1>
        </Col>
        <Col
          span={12}
          className="d-flex justify-content-end gap-5 align-items-center"
        >
          <h5 className="mb-0">Tháng</h5>
          {thisMonth && (
            <CustomInputSelect
              options={month}
              defaultValue={thisMonth}
              className="select-time"
              onChange={handleChange}
            />
          )}
          <CusButton label="Xuất file" className="btn-export" />
        </Col>
      </Row>
      <Row gutter={[16, 8]} className="mb-4">
        <Col span={8}>
          <StatisticsCard
            title="Doanh thu"
            money={statistic?.thisMonth?.revenue}
            compare={statistic?.lastMonth?.revenue}
          />
        </Col>
        <Col span={8}>
          <StatisticsCard
            title="Chi phí"
            money={100000000}
            compare={100000000}
          />
        </Col>
        <Col span={8}>
          <StatisticsCard
            title="Lợi nhuận"
            money={statistic?.thisMonth?.profit}
            compare={statistic?.lastMonth?.profit}
          />
        </Col>
      </Row>
      <Chart dataChart={dataChart} />
    </>
  );
};

export default Dashboard;
