import { Col, Form, Modal, Row, Space, Table } from "antd";
import { ColumnsType } from "antd/es/table";
import { useEffect, useState } from "react";
import { useAppDispatch } from "../../redux/store";
import { getAllUser } from "../../redux/features/userSlice";
import { useSelector } from "react-redux";
import CusButton from "../../components/CusButton/CusButton";
import "./User.scss";
import CustomInput from "../../components/CustomInput/CustomInput";
import CustomInputSelect from "../../components/CustomInputSelect/CustomInputSelect";
import { useForm } from "antd/es/form/Form";
import userService from "../../redux/api/userService";

interface IUser {
  _id: string;
  firstName: string;
  lastName: string;
  email: string;
  mobile: string;
  role: string;
  isBlocked: boolean;
}

const User = () => {
  const dispatch = useAppDispatch();
  const [isOpen, setIsOpen] = useState<boolean>(false);
  const [currentPage, setCurrentPage] = useState(1);
  const [user, setUser] = useState<IUser>();
  const [isEdit, setIsEdit] = useState<boolean>(false);
  const [roleForm] = useForm();

  const columns: ColumnsType<IUser> = [
    {
      title: "Tên người dùng",
      key: "lastName",
      render: (user: IUser) => `${user.firstName} ${user.lastName}`,
    },
    {
      title: "Email",
      dataIndex: "email",
      key: "email",
    },
    {
      title: "Số điện thoại",
      dataIndex: "mobile",
      key: "mobile",
    },
    {
      title: "Quyền",
      dataIndex: "role",
      filters: [
        {
          text: "admin",
          value: "admin",
        },
        {
          text: "user",
          value: "user",
        },
        {
          text: "employee",
          value: "employee",
        },
      ],
      onFilter: (value: string | number | boolean, record: IUser) =>
        record.role === value,
      sorter: (a, b) => a.role.length - b.role.length,
      sortDirections: ["ascend", "descend"],
    },
    {
      title: "Action",
      key: "",
      render: (_, record) => (
        <Space size="middle">
          <CusButton label="Xem chi tiết" onClick={() => modalUser(record)} />
          <CusButton onClick={() => {}} label="Xóa" />
        </Space>
      ),
      align: "center",
    },
  ];

  const modalUser = (user: IUser) => {
    setUser(user);
    setIsOpen(true);
  };

  useEffect(() => {
    dispatch(getAllUser(currentPage));
  }, [currentPage]);

  const { users, isLoading } = useSelector((state: any) => state.user);

  const onFinish = async (value: any) => {
    if (user) {
      await userService.changeRoleUser(user._id, value);
      dispatch(getAllUser(currentPage));
    }
  };

  return (
    <div>
      {isLoading && <div id="preloader"></div>}
      <Row className="m-2 mb-4">
        <Col span={24}>
          <h1 className="title-page">Người Dùng</h1>
        </Col>
      </Row>
      <Table
        columns={columns}
        dataSource={users?.users}
        rowKey={(record) => record.email}
        pagination={{
          pageSize: 10,
          current: currentPage,
          onChange: (e) => {
            setCurrentPage(e);
          },
          total: users?.count,
          showSizeChanger: false,
        }}
      />
      {user && (
        <Modal
          open={isOpen}
          footer={false}
          onCancel={() => {
            setIsOpen(false);
            setIsEdit(false);
          }}
          width={800}
          maskClosable={false}
        >
          {!isEdit ? (
            <div>
              <h4>{`Người dùng ${user.email}`}</h4>
              <div className="d-flex gap-10">
                <span className="label">Họ và tên: </span>
                <span className="content">
                  {user.firstName} {user.lastName}
                </span>
              </div>
              <div className="d-flex gap-10">
                <span className="label">Số điện thoại: </span>
                <span className="content">{user.mobile}</span>
              </div>
              <div className="d-flex gap-10">
                <span className="label">Quyền: </span>
                <span className="content">{user.role}</span>
              </div>
              <div className="d-flex justify-content-center mt-5">
                <CusButton
                  className="btn-edit"
                  label="Sửa"
                  onClick={() => {
                    setIsEdit(true);
                    roleForm.setFieldsValue({
                      name: `${user.firstName} ${user.lastName}`,
                      mobile: `${user.mobile}`,
                      role: `${user.role}`,
                    });
                  }}
                />
              </div>
            </div>
          ) : (
            <div>
              <h4>{`Người dùng ${user.email}`}</h4>
              <Form onFinish={onFinish} form={roleForm}>
                <div className="d-flex gap-10 mt-3">
                  <span className="label">Họ và tên: </span>
                  <Form.Item name="name" rules={[{ required: true }]}>
                    <CustomInput />
                  </Form.Item>
                </div>
                <div className="d-flex gap-10 mt-3">
                  <span className="label">Số điện thoại: </span>
                  <Form.Item name="mobile" rules={[{ required: true }]}>
                    <CustomInput />
                  </Form.Item>
                </div>
                <div className="d-flex gap-10 mt-3">
                  <span className="label">Quyền: </span>
                  <Form.Item name="role" rules={[{ required: true }]}>
                    <CustomInputSelect
                      options={[
                        { value: "user", label: "user" },
                        { value: "employee", label: "employee" },
                        { value: "admin", label: "admin" },
                      ]}
                    />
                  </Form.Item>
                </div>
                <div className="d-flex justify-content-center gap-30 mt-5">
                  <CusButton
                    className="btn-cancel"
                    label="Hủy"
                    onClick={() => setIsEdit(false)}
                  />
                  <CusButton
                    className="btn-edit"
                    label="Sửa"
                    htmlType="submit"
                  />
                </div>
              </Form>
            </div>
          )}
        </Modal>
      )}
    </div>
  );
};

export default User;
