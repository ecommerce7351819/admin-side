import { UserOutlined, LockOutlined } from "@ant-design/icons";
import { Checkbox, Form } from "antd";
import { Link, useNavigate } from "react-router-dom";
import CustomInput from "../../components/CustomInput/CustomInput";
import { getThisUser, login } from "../../redux/features/authSlice";
import { useAppDispatch } from "../../redux/store";

const Login = () => {
  const dispatch = useAppDispatch();
  const handleSubmit = async (value: any) => {
    await dispatch(login(value));
    await dispatch(getThisUser());
  };

  return (
    <div className="py-3 d-flex align-items-center login-page">
      <div className="my-5 mx-auto w-35 bg-white p-4 rounded-3 box-shadow">
        <h3 className="text-center fw-bold">Đăng Nhập</h3>
        <p className="text-center">
          Đăng nhập vào tài khoản của bạn để tiếp tục
        </p>
        <Form onFinish={handleSubmit}>
          <Form.Item
            name="email"
            rules={[
              { required: true, message: "Vui lòng nhập email của bạn!" },
              { type: "email", message: "Vui lòng nhập đúng định dạng email!" },
            ]}
          >
            <CustomInput
              type="text"
              label="Email"
              id="email"
              className="my-2"
              prefix={<UserOutlined />}
            />
          </Form.Item>
          <Form.Item
            name="password"
            rules={[
              { required: true, message: "Vui lòng nhập mật khẩu của bạn!" },
            ]}
          >
            <CustomInput
              type="password"
              label="Mật Khẩu"
              id="pass"
              prefix={<LockOutlined />}
              className="my-2"
            />
          </Form.Item>

          <div className="d-flex justify-content-between my-2">
            <Checkbox>Ghi nhớ đăng nhập</Checkbox>
            <Link to="/forgot-password">Quên mật khẩu</Link>
          </div>
          <button
            className="border-0 px-3 py-2 text-white fw-bold w-100 mt-3"
            type="submit"
            style={{ background: "#ffd333" }}
          >
            Đăng Nhập
          </button>
        </Form>
      </div>
    </div>
  );
};

export default Login;
