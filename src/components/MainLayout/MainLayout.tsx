import { useState, useEffect } from "react";
import { MenuUnfoldOutlined, MenuFoldOutlined } from "@ant-design/icons";
import { Button, Dropdown, Layout, Menu, MenuProps, theme } from "antd";
import "./MainLayout.scss";
import menus from "./menus.js";
import { Outlet, useLocation, useNavigate } from "react-router-dom";
import { IoNotificationsSharp } from "react-icons/io5";
import { useAppDispatch } from "../../redux/store";
import { useSelector } from "react-redux";
import { getThisUser } from "../../redux/features/authSlice";

const { Header, Sider, Content } = Layout;

const MainLayout = () => {
  const dispatch = useAppDispatch();
  const location = useLocation();
  const navigate = useNavigate();
  const [collapsed, setCollapsed] = useState(false);
  const {
    token: { colorBgContainer },
  } = theme.useToken();

  const onClick: MenuProps["onClick"] = ({ key }) => {
    if (key === "logout") {
      localStorage.removeItem("accessToken");
      window.location.reload();
    }
  };

  const items: MenuProps["items"] = [
    {
      label: "1st menu item",
      key: "1",
    },
    {
      label: "2nd menu item",
      key: "2",
    },
    {
      label: "Đăng xuất",
      key: "logout",
    },
  ];

  const key = location.pathname.split("/").pop();

  const { isLoading, user } = useSelector((state: any) => state.auth);

  return (
    <>
      {isLoading && <div id="preloader"></div>}
      <Layout>
        <Sider trigger={null} collapsible collapsed={collapsed} width={250}>
          <div className="logo">
            {collapsed ? (
              <p className="name-logo">L</p>
            ) : (
              <p className="name-logo">Longstore</p>
            )}
          </div>
          <Menu
            theme="dark"
            mode="inline"
            items={menus}
            onClick={({ key }) => {
              navigate(key);
            }}
            defaultSelectedKeys={key ? [key] : [""]}
          />
        </Sider>
        <Layout>
          <Header
            style={{ padding: 0, background: colorBgContainer }}
            className="d-flex justify-content-between"
          >
            <div className="d-flex gap-10 align-items-center">
              <Button
                type="text"
                icon={collapsed ? <MenuUnfoldOutlined /> : <MenuFoldOutlined />}
                onClick={() => setCollapsed(!collapsed)}
                className="btn-collapsed"
                style={{
                  fontSize: "16px",
                  width: 64,
                  height: 64,
                }}
              />
              <div className="search-bar">
                <input type="text" placeholder="Tìm kiếm..." />
                <button type="submit">Tìm</button>
              </div>
            </div>
            <div className="d-flex align-items-center">
              <div className="noti h-100 position-relative cursor-pointer">
                <IoNotificationsSharp className="fs-4" />
                <span className="badge bg-warning rounded-circle p-1 position-absolute">
                  3
                </span>
              </div>
              <Dropdown menu={{ items, onClick }}>
                <div className="toolbar-user d-flex justify-content-center align-items-center mr-3 h-100 cursor-pointer">
                  <img
                    className="avt"
                    src="https://stroyka-admin.html.themeforest.scompiler.ru/variants/ltr/images/customers/customer-4-64x64.jpg"
                    alt="img"
                  />
                  <div>
                    <h5 className="admin-info">
                      {user.firstName} {user.lastName}
                    </h5>
                    <div className="admin-info">{user.email}</div>
                  </div>
                </div>
              </Dropdown>
            </div>
          </Header>
          <Content
            style={{
              padding: "25px",
              background: "#f5f7fa",
            }}
          >
            <Outlet />
          </Content>
        </Layout>
      </Layout>
    </>
  );
};

export default MainLayout;
