import {
  DashboardOutlined,
  ShoppingCartOutlined,
  UnorderedListOutlined,
  UserOutlined,
} from "@ant-design/icons";
import { FaBlog } from "react-icons/fa";

function getItem(label, key, icon, children) {
  return {
    label,
    key,
    icon,
    children,
  };
}
const menus = [
  getItem("Dashboard", "", <DashboardOutlined />),
  getItem("Người Dùng", "users", <UserOutlined />),
  getItem("Danh Sách", "sub1", <UnorderedListOutlined />, [
    getItem("Quản lý sản phẩm", "product-list"),
    getItem("Quản lý nhãn hàng", "brand-list"),
    getItem("Quản lý danh mục", "categories"),
    getItem("Quản lý banner", "banners"),
    getItem("Quản lý mã giảm giá", "coupons"),
  ]),
  getItem("Đơn Hàng", "orders", <ShoppingCartOutlined />),
  getItem("Blog", "sub2", <FaBlog />, [getItem("Quản lý blog", "blog-list")]),
];

export default menus;
