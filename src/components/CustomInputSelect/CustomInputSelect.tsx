import { Select } from "antd";

const CustomInputSelect = ({ className, ...props }: any) => {
  return (
    <>
      <Select
        className={`cus-select ${className}`}
        style={{ width: 240 }}
        {...props}
      />
    </>
  );
};

export default CustomInputSelect;
