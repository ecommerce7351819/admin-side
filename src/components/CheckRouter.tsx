import React, { useEffect } from "react";
import { PRIVATE_ROUTER, PUBLIC_ROUTER } from "../config/constant";
import { useSelector } from "react-redux";
import { Navigate } from "react-router-dom";
import { getThisUser } from "../redux/features/authSlice";
import { useAppDispatch } from "../redux/store";

interface IPros {
  type: string;
  children: any;
}

const CheckRouter = (props: IPros) => {
  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(getThisUser());
  }, []);

  const { user } = useSelector((state: any) => state.auth);

  let check = Object.keys(user).length !== 0;

  let component = props.children;

  if (
    (check === true && props.type === PRIVATE_ROUTER) ||
    (check === false && props.type === PUBLIC_ROUTER)
  ) {
    return component;
  } else if (check === true) {
    return <Navigate to="/" />;
  } else if (check === false && props.type === PRIVATE_ROUTER) {
    return <Navigate to="/login" replace />;
  }
};

export default CheckRouter;
