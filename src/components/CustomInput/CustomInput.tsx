import { Input } from "antd";
import "./CustomInput.scss";

const CustomInput = (props: any) => {
  const { type, label, i_id, i_class, isSearch, ...rest } = props;

  return (
    <Input
      type={type}
      id={i_id}
      placeholder={label}
      className={`custom-input ${i_class}`}
      {...rest}
    />
  );
};

export default CustomInput;
