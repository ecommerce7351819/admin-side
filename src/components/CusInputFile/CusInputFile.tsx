import React from "react";
import "./CusInputFile.scss";

const CusInputFile = (prop: any) => {
  const { ...rest } = prop;

  return <input type="file" className="custom-file-input" {...rest} />;
};

export default CusInputFile;
