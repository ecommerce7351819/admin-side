import { Card } from "antd";
import {
  BarChart,
  Bar,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
} from "recharts";
import "./Chart.scss";

const Chart = (props: any) => {
  const { dataChart } = props;
  return (
    <Card className="box-shadow">
      <h2 className="title-card mb-4">Thống Kê Doanh Thu</h2>
      <BarChart
        width={1100}
        height={300}
        data={dataChart}
        margin={{
          top: 5,
          right: 30,
          left: 20,
          bottom: 5,
        }}
      >
        <CartesianGrid strokeDasharray="3 3" />
        <XAxis dataKey="date" />
        <YAxis />
        <Tooltip />
        <Legend />
        <Bar dataKey="revenue" fill="#8884d8" />
        <Bar dataKey="profit" fill="#82ca9d" />
      </BarChart>
    </Card>
  );
};

export default Chart;
