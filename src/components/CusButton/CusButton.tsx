import { Button } from "antd";
import "./CusButton.scss";

const CusButton = (props: any) => {
  const { label, type, loading, className, ...rest } = props;

  return (
    <>
      <Button
        className={`cus-button ${className}`}
        htmlType={type}
        loading={loading}
        {...rest}
      >
        {label}
      </Button>
    </>
  );
};

export default CusButton;
