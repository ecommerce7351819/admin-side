import "./App.scss";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Login from "./pages/Login/Login";
import ResetPassword from "./pages/ResetPassword/ResetPassword";
import ForgotPassword from "./pages/ForgotPassword/ForgotPassword";
import MainLayout from "./components/MainLayout/MainLayout";
import Dashboard from "./pages/Dashboard/Dashboard";
import User from "./pages/User/User";
import CheckRouter from "./components/CheckRouter";
import { PRIVATE_ROUTER, PUBLIC_ROUTER } from "./config/constant";
import Product from "./pages/Product/Product";
import AddProduct from "./pages/Product/AddProduct";
import AddBrand from "./pages/Brand/AddBrand";
import Brand from "./pages/Brand/Brand";
import ProductCat from "./pages/ProductCat/ProductCat";
import AddProductCat from "./pages/ProductCat/AddProductCat";
import Banner from "./pages/Banner/Banner";
import AddBanner from "./pages/Banner/AddBanner";
import AddCoupon from "./pages/Coupon/AddCoupon";
import Coupon from "./pages/Coupon/Coupon";
import Order from "./pages/Order/Order";
import Blog from "./pages/Blog/Blog";
import AddBlog from "./pages/Blog/AddBlog";
import DetailProduct from "./pages/Product/DetailProduct";

function App() {
  return (
    <Router>
      <Routes>
        <Route
          path="/login"
          element={
            <CheckRouter type={PUBLIC_ROUTER}>
              <Login />
            </CheckRouter>
          }
        />
        <Route
          path="/reset-password"
          element={
            <CheckRouter type={PUBLIC_ROUTER}>
              <ResetPassword />
            </CheckRouter>
          }
        />
        <Route
          path="/forgot-password"
          element={
            <CheckRouter type={PUBLIC_ROUTER}>
              <ForgotPassword />
            </CheckRouter>
          }
        />
        <Route
          path="/"
          element={
            <CheckRouter type={PRIVATE_ROUTER}>
              <MainLayout />
            </CheckRouter>
          }
        >
          <Route index element={<Dashboard />} />
          <Route path="/users" element={<User />} />
          <Route path="/product-list" element={<Product />} />
          <Route path="/add-product" element={<AddProduct />} />
          <Route path="/product-detail/:slug" element={<DetailProduct />} />
          <Route path="/add-brand" element={<AddBrand />} />
          <Route path="/brand-list" element={<Brand />} />
          <Route path="/categories" element={<ProductCat />} />
          <Route path="/add-category" element={<AddProductCat />} />
          <Route path="/banners" element={<Banner />} />
          <Route path="/add-banner" element={<AddBanner />} />
          <Route path="/add-coupon" element={<AddCoupon />} />
          <Route path="/coupons" element={<Coupon />} />
          <Route path="/orders" element={<Order />} />
          <Route path="/blog-list" element={<Blog />} />
          <Route path="/add-blog" element={<AddBlog />} />
        </Route>
      </Routes>
    </Router>
  );
}

export default App;
