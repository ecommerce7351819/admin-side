import {configureStore} from '@reduxjs/toolkit'
import authReducer from './features/authSlice'
import userReducer from './features/userSlice'
import productReducer from './features/productSlice'
import uploadReducer from './features/uploadSlice'
import brandReducer from './features/brandSlice'
import bannerReducer from './features/bannerSlice'
import productCatReducer from './features/productCatSlice'
import couponReducer from './features/couponSlice'
import orderReducer from './features/orderSlice'
import blogRudecer from './features/blogSlice'
import { useDispatch } from 'react-redux'
import logger from 'redux-logger'

export const store = configureStore({
  reducer: {auth: authReducer, user: userReducer, product: productReducer, upload: uploadReducer,
            brand: brandReducer, productCat: productCatReducer, banner: bannerReducer,
            coupon: couponReducer, order: orderReducer, blog: blogRudecer
          },
  // middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(logger)
})

export type AppDispatch = typeof store.dispatch
export const useAppDispatch: () => AppDispatch = useDispatch 
