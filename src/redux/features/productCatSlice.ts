import { createAsyncThunk, createSlice } from "@reduxjs/toolkit"
import productCatService from "../api/productCatService";

interface IProductCat {
  title: string,
  img: string,
}

const initialState = {
  productCats: [] as IProductCat[],
  isError: false,
  isLoading: false,
  isSuccess: false,
  message: ""
}

export const getAllProductCat = createAsyncThunk('productCat/getAll', async(_, thunkAPI) => {
  try {
    return await productCatService.getAllProductCat();
  } catch (error) {
    return thunkAPI.rejectWithValue(error)
  }
})

export const productCat = createSlice({
  name: "productCat",
  initialState,
  reducers:{},
  extraReducers: (builder) => {
    builder
      .addCase(getAllProductCat.pending, (state) => {
        state.isLoading = true
      })
      .addCase(getAllProductCat.fulfilled, (state, action) => {
        state.isLoading = false;
        state.productCats = action.payload;
      })
      .addCase(getAllProductCat.rejected, (state) => {
        state.isError = true
        state.isLoading = false
      })
  }
})

export default productCat.reducer