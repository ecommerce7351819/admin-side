import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { Product } from "../api/types";
import productService from "../api/productService";

interface IProductState {
  products: Product[];
  isError: boolean;
  isLoading: boolean;
  isSuccess: boolean;
  message: string; 
}

const initialState: IProductState = {
  products: [],
  isError: false,
  isLoading: false,
  isSuccess: false,
  message: ""
}

export const getAllProduct = createAsyncThunk('product/getAll', async(page: number, thunkAPI) => {
  try {
    return await productService.getAllProduct(page)
  } catch (error) {
    return thunkAPI.rejectWithValue(error)
  }
})

export const productSlice = createSlice({
  name: "product",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(getAllProduct.pending, (state) => {
        state.isLoading = true;
      })
      .addCase(getAllProduct.fulfilled, (state, action) => {
        state.isLoading = false;
        state.products = action.payload;
      })
      .addCase(getAllProduct.rejected, (state) => {
        state.isError = true
        state.isLoading = false
      })
  }
})

export default productSlice.reducer