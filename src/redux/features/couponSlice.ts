import { createAsyncThunk, createSlice } from "@reduxjs/toolkit"
import couponService from "../api/couponService"

interface Icoupon {
  name: string;
  expiry: Date;
  discount: Number;
}

const initialState = {
  coupons: [] as Icoupon[],
  isError: false,
  isLoading: false,
  isSuccess: false,
  message: ""
}

export const getAllCoupon = createAsyncThunk('coupon/getAll', async(_, thunkAPI) => {
  try {
    return await couponService.getAllCoupon();
  } catch (error) {
    return thunkAPI.rejectWithValue(error)
  }
})

export const couponSlice = createSlice({
  name: "coupon",
  initialState,
  reducers:{},
  extraReducers: (builder) => {
    builder
      .addCase(getAllCoupon.pending, (state) => {
        state.isLoading = true
      })
      .addCase(getAllCoupon.fulfilled, (state, action) => {
        state.isLoading = false;
        state.coupons = action.payload;
      })
      .addCase(getAllCoupon.rejected, (state) => {
        state.isError = true
        state.isLoading = false
      })
  }
})

export default couponSlice.reducer