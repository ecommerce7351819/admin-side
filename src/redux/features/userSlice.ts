import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import userService from "../api/userService";

const initialState = {
  users: null,
  isLoading: false,
  isError: false,
  isSuccess: false,
  messagr: ""
}

export const getAllUser = createAsyncThunk('users/getAll',async (data: number, thunkAPI) => {
  try {
    return await userService.getAllUser(data)
  } catch (error) {
    return thunkAPI.rejectWithValue(error)
  }
})

export const userSlice = createSlice({
  name: 'users',
  initialState,
  reducers:{},
  extraReducers: (builder) => {
    builder
      .addCase(getAllUser.pending, (state) => {
        state.isLoading = true
      })
      .addCase(getAllUser.fulfilled, (state, action) => {
        state.isLoading = false
        state.isSuccess = true
        state.users = action.payload
      })
      .addCase(getAllUser.rejected, (state) => {
        state.isError = true
        state.isLoading = false
      })
  }
})

export default userSlice.reducer