import { createAsyncThunk, createSlice } from "@reduxjs/toolkit"
import orderService from "../api/orderService"

export interface IOrder {
  _id: any;
  cart: any;
  info: any;
  paymentIntent: string;
  orderStatus: string;
  orderby: string;
  createdAt: string;
}

const initialState = {
  orders: [] as IOrder[],
  isError: false,
  isLoading: false,
  isSuccess: false,
  message: ""
}

export const getAllOrder = createAsyncThunk('order/getAll', async (page: number, thunkAPI) => {
  try {
    return await orderService.getAllOrder(page);
  } catch (error) {
    return thunkAPI.rejectWithValue(error)
  }
})

export const orderSlice = createSlice({
  name: "order",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(getAllOrder.pending, (state) => {
        state.isLoading = true
      })
      .addCase(getAllOrder.fulfilled, (state, action) => {
        state.isLoading = false;
        state.orders = action.payload;
      })
      .addCase(getAllOrder.rejected, (state) => {
        state.isError = true
        state.isLoading = false
      })
  }
})

export default orderSlice.reducer