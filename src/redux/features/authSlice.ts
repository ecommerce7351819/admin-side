import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import authService from "../api/authService";
import { Admin } from "../api/types";

interface AuthState {
  user: Admin;
  isError: boolean;
  isLoading: boolean;
  isSuccess: boolean;
  message: string;
}

const initialState: AuthState = {
  user: {} as Admin,
  isError: false,
  isLoading: false,
  isSuccess: false,
  message: "",
};

export const login = createAsyncThunk(
  'auth/admin-login',
  async (body: { email: string; password: string }, thunkAPI) => {
    try {
      return await authService.login(body);
    } catch (error) {
      return thunkAPI.rejectWithValue(error);
    }
  }
);

export const getThisUser = createAsyncThunk("auth/get-this", async(data, thunkAPI) => {
  try {
    const res = await authService.getThisUser();
    return res
  } catch (error) {
    return thunkAPI.rejectWithValue(error)
  }
})

export const authSlice = createSlice({
  name: "auth",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(getThisUser.pending, (state) => {
        state.isLoading = true;
      })
      .addCase(getThisUser.fulfilled, (state, action) => {
        state.isLoading = false;
        state.isSuccess = true;
        state.user = action.payload;
      })
      .addCase(getThisUser.rejected, (state, action) => {
        state.isLoading = false;
        state.isError = true;
        state.isSuccess = false;
        state.user = {} as Admin;
      });
  },
});

export default authSlice.reducer