import { createAsyncThunk, createSlice } from "@reduxjs/toolkit"
import bannerService from "../api/bannerService"

interface IBanner {
  title: string,
  img: any,
  action: string,
  isShow: boolean
}

const initialState = {
  banners: [] as IBanner[],
  isError: false,
  isLoading: false,
  isSuccess: false,
  message: ""
}

export const getAllBanner = createAsyncThunk('banner/getAll', async(_, thunkAPI) => {
  try {
    return await bannerService.getAllBanner();
  } catch (error) {
    return thunkAPI.rejectWithValue(error)
  }
})

export const bannerSlice = createSlice({
  name: "banner",
  initialState,
  reducers:{},
  extraReducers: (builder) => {
    builder
      .addCase(getAllBanner.pending, (state) => {
        state.isLoading = true
      })
      .addCase(getAllBanner.fulfilled, (state, action) => {
        state.isLoading = false;
        state.banners = action.payload;
      })
      .addCase(getAllBanner.rejected, (state) => {
        state.isError = true
        state.isLoading = false
      })
  }
})

export default bannerSlice.reducer