import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import uploadService from "../api/uploadSevice";

const initialState = {
  mainImg: [] as any[],
  imgs: [] as any[],
  isLoading: false,
  isError: false,
  isSuccess: false,
  message: ""
}

export const uploadImg = createAsyncThunk('upload/imgs',async (data: any, thunkAPI) => {
  try {
    const formData = new FormData();
    for (let i = 0; i < data.length; i++) {
      formData.append("images", data[i])
    }
    return await uploadService.upload(formData)
  } catch (error) {
    return thunkAPI.rejectWithValue(error)
  }
})

export const uploadMainImg = createAsyncThunk('upload/main-img',async (data: any, thunkAPI) => {
  try {
    const formData = new FormData();
    for (let i = 0; i < data.length; i++) {
      formData.append("images", data[i])
    }
    return await uploadService.upload(formData)
  } catch (error) {
    return thunkAPI.rejectWithValue(error)
  }
})

export const deleteImg = createAsyncThunk('delete/imgs',async (id: any, thunkAPI) => {
  try {
    return await uploadService.deleteImg(id)
  } catch (error) {
    return thunkAPI.rejectWithValue(error)
  }
})

export const uploadSlice = createSlice({
  name: 'images',
  initialState,
  reducers:{},
  extraReducers: (builder) => {
    builder
      .addCase(uploadImg.pending, (state) => {
        state.isLoading = true
      })
      .addCase(uploadImg.fulfilled, (state, {payload}) => {
        state.isLoading = false
        state.isSuccess = true
        state.imgs = state.imgs.concat(payload)
      })
      .addCase(uploadImg.rejected, (state, action) => {
        state.isError = true
        state.isLoading = false
      })
      .addCase(uploadMainImg.pending, (state) => {
        state.isLoading = true
      })
      .addCase(uploadMainImg.fulfilled, (state, action) => {
        state.isLoading = false
        state.isSuccess = true
        state.mainImg = action.payload
      })
      .addCase(uploadMainImg.rejected, (state, action) => {
        state.isError = true
        state.isLoading = false
      })
      .addCase(deleteImg.pending, (state) => {
        state.isLoading = true
      })
      .addCase(deleteImg.fulfilled, (state, action) => {
        state.isLoading = false
        state.isSuccess = true
      })
      .addCase(deleteImg.rejected, (state, action) => {
        state.isError = true
        state.isLoading = false
      })
  }
})

export default uploadSlice.reducer