import { createAsyncThunk, createSlice } from "@reduxjs/toolkit"
import brandService from "../api/brandService"

interface IBrand {
  title: string,
  img: string,
}

const initialState = {
  brands: [] as IBrand[],
  isError: false,
  isLoading: false,
  isSuccess: false,
  message: ""
}

export const getAllBrand = createAsyncThunk('brand/getAll', async(_, thunkAPI) => {
  try {
    return await brandService.getAllBrand();
  } catch (error) {
    return thunkAPI.rejectWithValue(error)
  }
})

export const brandSlice = createSlice({
  name: "brand",
  initialState,
  reducers:{},
  extraReducers: (builder) => {
    builder
      .addCase(getAllBrand.pending, (state) => {
        state.isLoading = true
      })
      .addCase(getAllBrand.fulfilled, (state, action) => {
        state.isLoading = false;
        state.brands = action.payload;
      })
      .addCase(getAllBrand.rejected, (state) => {
        state.isError = true
        state.isLoading = false
      })
  }
})

export default brandSlice.reducer