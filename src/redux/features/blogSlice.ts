import { createAsyncThunk, createSlice } from "@reduxjs/toolkit"
import blogService from "../api/blogService"

interface IBlog {
  title: string,
  img: string,
}

const initialState = {
  blogs: [] as IBlog[],
  isError: false,
  isLoading: false,
  isSuccess: false,
  message: ""
}

export const getAllBlog = createAsyncThunk('blog/getAll', async(_, thunkAPI) => {
  try {
    return await blogService.getAllBlog();
  } catch (error) {
    return thunkAPI.rejectWithValue(error)
  }
})

export const blogSlice = createSlice({
  name: "blog",
  initialState,
  reducers:{},
  extraReducers: (builder) => {
    builder
      .addCase(getAllBlog.pending, (state) => {
        state.isLoading = true
      })
      .addCase(getAllBlog.fulfilled, (state, action) => {
        state.isLoading = false;
        state.blogs = action.payload;
      })
      .addCase(getAllBlog.rejected, (state) => {
        state.isError = true
        state.isLoading = false
      })
  }
})

export default blogSlice.reducer