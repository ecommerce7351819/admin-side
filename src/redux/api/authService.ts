import axios from '../../config/axiosConfig';


const login = async (body: { email: string; password: string }) => {
  const res = await axios.post('user/admin-login', body)
  localStorage.setItem("accessToken", res.data.token)
  return res.data
}

const getThisUser = async () => {
  const res = await axios.get("user/this-admin")
  return res.data
}

const authService = {login, getThisUser}

export default authService