import axios from "../../config/axiosConfig"

const getAllBanner = async () => {
  const res = await axios.get('/banner/list');
  return res.data;
}

const createBanner = async (data: any) => {
  const res = await axios.post('/banner/create', data);
  return res.data
}

const deleteBanner = async (id: any) => {
  const res = await axios.delete(`/banner/${id}`);
  return res.data
}

const bannerService = {getAllBanner,createBanner,deleteBanner}

export default bannerService