import axios from "../../config/axiosConfig";


const getAllProductCat = async () => {
  const res = await axios.get('/category/list');
  return res.data
}

const createProductCat = async (data: any) => {
  const res = await axios.post('/category/create', data);
  return res.data
}

const deleteProductCat = async (id: any) => {
  const res = await axios.delete(`/category/${id}`);
  return res.data
}

const productCatService = {getAllProductCat,createProductCat,deleteProductCat}

export default productCatService