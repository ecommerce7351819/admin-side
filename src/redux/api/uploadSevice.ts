import axios from '../../config/axiosConfig';

const upload = async (data: any) => {
  const formData = new FormData();
    for (let i = 0; i < data.length; i++) {
      formData.append("images", data[i])
    }
  const res = await axios.post("/upload", formData)
  return res.data
}

const deleteImg = async(id: any) => {
  const res = await axios.delete(`/upload/delete-img/${id}`)
  return res.data
}

const uploadService = {upload,deleteImg}

export default uploadService