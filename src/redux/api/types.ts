export type Admin = {
  _id: any;
  firstname: string;
  lastname: string;
  email: string;
  token: string;
  mobile: string;
}

export type User = {
  _id: any;
  firstname: string;
  lastname: string;
  email: string;
  mobile: string;
  role: string;
  isBlocked: boolean
} 

export type Product = {
  _id: any;
  title: string;
  slug: string;
  description: any;
  price: number;
  category: [];
  brand: string;
  quantity: number;
  sold: number;
  images: [];
  color: [];
  tags: [];
  ratings: [];
  totalRating: string;
}