import axios  from "../../config/axiosConfig";

const getStatistic = async () => {
  const res = await axios.get("/statistic/get-all");
  return res.data
}

const getStatisticByMonth = async (data: any) => {  
  const res = await axios.post("/statistic/get-by-month", data);
  return res.data
}

const statisticService = {getStatistic, getStatisticByMonth}

export default statisticService