import axios  from "../../config/axiosConfig";

const getAllProduct = async (page: number) => {
  const res = await axios.get(`/product?page=${page}`);
  return res.data
}

const createProduct = async (data: any) => {
  const res = await axios.post('/product/create', data);
  return res.data
}

const updateProduct = async (id: any, data: any) => {
  const res = await axios.put(`/product/update/${id}`, data);
  return res.data
}

const getAProduct = async (slug: any) => {
  try {
    const res = await axios.get(`/product/get-product/${slug}`);
    return res.data;
  } catch (error) {
    console.log(error);
  }
};

const productService = {getAllProduct, createProduct, getAProduct, updateProduct}

export default productService