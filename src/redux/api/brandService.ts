import axios from "../../config/axiosConfig"

const getAllBrand = async () => {
  const res = await axios.get('/brand/list');
  return res.data;
}

const createBrand = async (data: any) => {
  const res = await axios.post('/brand/create', data);
  return res.data
}

const deleteBrand = async (id: any) => {
  const res = await axios.delete(`/brand/${id}`);
  return res.data
}

const brandService = {getAllBrand,createBrand,deleteBrand}

export default brandService