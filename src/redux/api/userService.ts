import axios from '../../config/axiosConfig';

const getAllUser = async (page: number) => {
  const res = await axios.get(`/user?page=${page}`);
  return res.data
}

const changeRoleUser = async (id: string, data: any) => {
  const res = await axios.put(`/user/change-role/${id}`, data);
  return res.data;
}

const userService = { getAllUser, changeRoleUser }

export default userService