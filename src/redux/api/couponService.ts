import axios from "../../config/axiosConfig"

const getAllCoupon = async () => {
  const res = await axios.get('/coupon/list');
  return res.data;
}

const createCoupon = async (data: any) => {
  const res = await axios.post('/coupon/create', data);
  return res.data
}

const deleteCoupon = async (id: any) => {
  const res = await axios.delete(`/coupon/${id}`);
  return res.data
}

const couponService = {getAllCoupon,createCoupon,deleteCoupon}

export default couponService