import axios from "../../config/axiosConfig"

const getAllBlog = async () => {
  const res = await axios.get('/blog/list');
  return res.data;
}

const createBlog = async (data: any) => {
  const res = await axios.post('/blog/create', data);
  return res.data
}

const deleteBlog = async (id: any) => {
  const res = await axios.delete(`/blog/${id}`);
  return res.data
}

const blogService = {getAllBlog,createBlog,deleteBlog}

export default blogService