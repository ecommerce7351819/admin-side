import axios from "../../config/axiosConfig";

const createOrder = async (data: any) => {
  const res = await axios.post("order/create", data);
  return res.data;
};

const getAllOrder = async (page: number) => {
  const res = await axios.get(`order/list?page=${page}`);
  return res.data;
};

const getAllOrderByUser = async (id: string) => {
  const res = await axios.get(`order/orders/${id}`);
  return res.data;
};

const changeOrderStatus = async (id: string, data: any) => {
  const res = await axios.put(`order/change-status/${id}`, data);
  return res.data;
};

const handleChangePay = async (id: string, data: any) => {
  const res = await axios.put(`order/change-pay/${id}`, data);
  return res.data;
};

const orderService = { createOrder, getAllOrder, getAllOrderByUser, changeOrderStatus, handleChangePay };

export default orderService;
