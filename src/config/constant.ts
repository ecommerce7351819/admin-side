export const BASE_API_URL = "http://localhost:8888/api/";
export const API_URL = "https://longstore-api.onrender.com/api";

export const PRIVATE_ROUTER = "private-router";
export const PUBLIC_ROUTER = "public-router";
